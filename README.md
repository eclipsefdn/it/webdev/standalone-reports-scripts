# standalone-reports-scripts

## Usage

With a local running LDAP instance, the following commands can be used to run the various reports provided by this repository.

### Spam report

This report will read all of the disposable email domains from a [popular utility project in Github](https://github.com/unkn0w/disposable-email-domain-list) and iterate over each, looking in the LDAP instance for mail addresses matching the given domains. These entries will then be pulled and formatted into a report item to be printed to the current working directory at the end of the run. Additionally, there is some effort made to try and extract the groups where possible to give better indicators whether the user is legitimate.

To use this repo, connections to 2 services are required. The first is a connection to the `eclipse` database to look up user session logs, which are used to indicate last login for a user. This will be configured in `.env` file to simplify the process and to remove the need to enter the values into a file that might accidentally get pushed to the repo. A sample environment file is provided under `/config/.env`, and should be copied to the root of the project and updated to reflect the current environment.

The second is a tunnel to the LDAP instance that should be scanned for spam entries. Currently, these are configured through the command line call that runs the report below, where all of the CLI flags in the `quarkus.args` argument should be updated to reflect the tunneled/local environment.

Running the below command will start a dev server that will run the report locally and when done will notify the console. During the run, there are progress indicators that will be provided to the console to help gauge the current progress of the report.

```
source .env && mvn compile quarkus:dev -Dquarkus.args="spam-report --basedn=dc=example,dc=org --host=localhost --port=389 --binddn=cn=admin,dc=example,dc=org --password=<password here>"
```

### Foundation report

TBD