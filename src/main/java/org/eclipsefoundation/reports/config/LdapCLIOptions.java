/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.config;

import picocli.CommandLine;

/**
 * Shared LDAP CLI options to be used as a mixin for picocli commands.
 * 
 * @author Martin Lowe
 *
 */
public class LdapCLIOptions {

    // settings for LDAP connection
    @CommandLine.Option(names = { "-h", "--host" }, description = "Host for the LDAP", defaultValue = "localhost")
    String host;
    @CommandLine.Option(names = { "-p", "--port" }, description = "Port for the LDAP connection", defaultValue = "389")
    Integer port;
    @CommandLine.Option(names = { "-d", "--basedn" }, description = "Base DN for LDAP query", defaultValue = "dc=eclipse,dc=org")
    String baseDN;
    @CommandLine.Option(names = { "-B", "--binddn" }, description = "Bind DN for LDAP query")
    String bindDN;
    @CommandLine.Option(names = { "-P", "--password" }, description = "Password to authenticate for LDAP query", interactive = true)
    String password;

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @return the port
     */
    public Integer getPort() {
        return port;
    }

    /**
     * @return the baseDN
     */
    public String getBaseDN() {
        return baseDN;
    }

    /**
     * @return the bindDN
     */
    public String getBindDN() {
        return bindDN;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

}
