/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.dtos;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;

/**
 * Event log for EF to track system events.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table(name = "SYS_EvtLog")
public class EvtLog extends PanacheEntityBase {

    @Id
    private int logId;
    private String logTable;
    @Column(name = "PK1")
    private String pk1;
    @Column(name = "PK2")
    private String pk2;
    private String logAction;
    private String uid;
    private LocalDateTime evtDateTime;

    /**
     * Finds the most recent login event session if it exists
     * @param uid
     * @return
     */
    public static EvtLog findLastLoginSession(String uid) {
        return find("logTable = 'sessions' AND uid = ?1", Sort.descending("evtDateTime"), uid).firstResult();
    }

    /**
     * @return the logId
     */
    public int getLogId() {
        return logId;
    }

    /**
     * @param logId the logId to set
     */
    public void setLogId(int logId) {
        this.logId = logId;
    }

    /**
     * @return the logTable
     */
    public String getLogTable() {
        return logTable;
    }

    /**
     * @param logTable the logTable to set
     */
    public void setLogTable(String logTable) {
        this.logTable = logTable;
    }

    /**
     * @return the pk1
     */
    public String getPk1() {
        return pk1;
    }

    /**
     * @param pk1 the pk1 to set
     */
    public void setPk1(String pk1) {
        this.pk1 = pk1;
    }

    /**
     * @return the pk2
     */
    public String getPk2() {
        return pk2;
    }

    /**
     * @param pk2 the pk2 to set
     */
    public void setPk2(String pk2) {
        this.pk2 = pk2;
    }

    /**
     * @return the logAction
     */
    public String getLogAction() {
        return logAction;
    }

    /**
     * @param logAction the logAction to set
     */
    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return the evtDateTime
     */
    public LocalDateTime getEvtDateTime() {
        return evtDateTime;
    }

    /**
     * @param evtDateTime the evtDateTime to set
     */
    public void setEvtDateTime(LocalDateTime evtDateTime) {
        this.evtDateTime = evtDateTime;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SysEventLog [logId=");
        builder.append(logId);
        builder.append(", logTable=");
        builder.append(logTable);
        builder.append(", pk1=");
        builder.append(pk1);
        builder.append(", pk2=");
        builder.append(pk2);
        builder.append(", logAction=");
        builder.append(logAction);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", evtDateTime=");
        builder.append(evtDateTime);
        builder.append("]");
        return builder.toString();
    }

}
