/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.api.models;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_EclipseMailingListsResponse.Builder.class)
public abstract class EclipseMailingListsResponse {

    public abstract List<MailingListSubscription> getMailingListSubscriptions();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_EclipseMailingListsResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setMailingListSubscriptions(List<MailingListSubscription> mailingListsSubscriptions);

        public abstract EclipseMailingListsResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EclipseMailingListsResponse_MailingListSubscription.Builder.class)
    public abstract static class MailingListSubscription {

        public abstract String getListName();

        public static Builder builder() {
            return new AutoValue_EclipseMailingListsResponse_MailingListSubscription.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setListName(String listName);

            public abstract MailingListSubscription build();
        }
    }
}