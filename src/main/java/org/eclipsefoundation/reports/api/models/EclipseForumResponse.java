/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.api.models;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_EclipseForumResponse.Builder.class)
public abstract class EclipseForumResponse {

    public abstract String getPostedMsgCount();

    public abstract List<ForumPost> getPosts();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_EclipseForumResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setPostedMsgCount(String query);

        public abstract Builder setPosts(List<ForumPost> posts);

        public abstract EclipseForumResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EclipseForumResponse_ForumPost.Builder.class)
    public abstract static class ForumPost {

        public abstract String getRootMsgSubject();

        public static Builder builder() {
            return new AutoValue_EclipseForumResponse_ForumPost.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setRootMsgSubject(String rootMsgSubject);

            public abstract ForumPost build();
        }
    }
}