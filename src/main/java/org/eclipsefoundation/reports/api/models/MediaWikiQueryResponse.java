/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.reports.api.models;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Entity representing a Working Group committee
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_MediaWikiQueryResponse.Builder.class)
public abstract class MediaWikiQueryResponse {

    public abstract QueryOutput getQuery();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_MediaWikiQueryResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setQuery(QueryOutput query);

        public abstract MediaWikiQueryResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MediaWikiQueryResponse_QueryOutput.Builder.class)
    public abstract static class QueryOutput {

        public abstract List<Usercontrib> getUsercontribs();

        public static Builder builder() {
            return new AutoValue_MediaWikiQueryResponse_QueryOutput.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setUsercontribs(List<Usercontrib> usercontribs);

            public abstract QueryOutput build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MediaWikiQueryResponse_Usercontrib.Builder.class)
    public abstract static class Usercontrib {

        public abstract String getUser();

        public abstract ZonedDateTime getTimestamp();

        public static Builder builder() {
            return new AutoValue_MediaWikiQueryResponse_Usercontrib.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setTimestamp(ZonedDateTime timestamp);

            public abstract Builder setUser(String user);

            public abstract Usercontrib build();
        }
    }
}
