/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.api;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.reports.api.models.MediaWikiQueryResponse;

/**
 * Client for interacting with the Eclipse mediawiki to look up whether users have mad contributions in the wiki as an
 * active user metric.
 */
@RegisterRestClient(baseUri = "https://wiki.eclipse.org")
public interface MediaWikiAPI {

    /**
     * Query the mediawiki instance to look up user contributions as defined in the param object.
     * 
     * @param param wrapped parameters used to query user contributions
     * @return the response from mediawiki for the query.
     */
    @GET
    @Path("api.php")
    MediaWikiQueryResponse query(@BeanParam MWQuery param);

    /**
     * Parameter wrapper for mediawiki queries, allowing us to look up specific user stats for requested actions.
     */
    public class MWQuery {
        @QueryParam("ucuser")
        public final String user;
        @QueryParam("action")
        public final String action;
        @QueryParam("list")
        public final String list;
        @QueryParam("format")
        public final String format;

        public MWQuery(String user) {
            this.user = user;
            this.action = "query";
            this.list = "usercontribs";
            this.format = "json";
        }
    }
}
