/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.api;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * API binding to fetch the raw disposable domain list from Github. Authorization is added but not currently used in
 * case this gets pushed to a shared environment where the unauthenticated rate limit may have been consumed in the
 * future.
 * 
 * @author Martin Lowe
 *
 */
@RegisterRestClient
@Produces("application/vnd.github.VERSION.raw")
public interface DisposableDomainGithubProjectRaw {

    @GET
    String getRawDomainList(@HeaderParam("Authorization") String authorization);
}
