/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.reports.api.models.EclipseForumResponse;
import org.eclipsefoundation.reports.api.models.EclipseMailingListsResponse;

/**
 * Connects to the Eclipse Foundation metadata links for users to lookup whether users have interacted with common
 * services, such as the forum and mailing list subscriptions.
 */
@RegisterRestClient(baseUri = "https://api.eclipse.org/account/profile")
public interface AccountsAPI {

    /**
     * Lookup forum post information for the given user.
     * 
     * @param user username of individual to lookup
     * @return list of forum posts for the passed user if they exist
     */
    @GET
    @Path("{user}/forum")
    EclipseForumResponse getForumPostsForUser(@PathParam("user") String user);


    /**
     * Lookup mailing list subscription information for the given user.
     * 
     * @param user username of individual to lookup
     * @return list of mailing list subscriptions for the passed user if they exist
     */
    @GET
    @Path("{user}/mailing-list")
    EclipseMailingListsResponse getMailingListForUser(@PathParam("user") String user);
}
