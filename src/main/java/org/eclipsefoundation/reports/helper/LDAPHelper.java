/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.helper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.reports.config.LdapCLIOptions;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;

/**
 * Basic helper to performe LDAP queries and connections for the currently running service.
 * 
 * @author Martin Lowe
 *
 */
public class LDAPHelper implements AutoCloseable {

    public static final List<String> BASE_SEARCH_ATTRIBUTES = Collections.unmodifiableList(Arrays.asList("uid", "mail"));

    private String password;
    private String host;
    private Integer port;
    private String baseDn;
    private String bindDn;

    private LDAPConnection connection;

    /**
     * @param password
     * @param host
     * @param port
     * @param bindDN
     */
    public LDAPHelper(LdapCLIOptions options) {
        this.password = options.getPassword();
        this.host = options.getHost();
        this.port = options.getPort();
        this.bindDn = options.getBindDN();
        this.baseDn = options.getBaseDN();
        this.connection = null;
    }

    public List<SearchResultEntry> search(Map<String, String> params, Optional<SearchScope> scope, Optional<List<String>> attrs)
            throws LDAPException {
        return search(params, "|", scope, attrs);
    }

    public List<SearchResultEntry> search(Map<String, String> params, String searchOp, Optional<SearchScope> scope,
            Optional<List<String>> attrs) throws LDAPException {
        SearchResult result = getConnection()
                .search(new SearchRequest(baseDn, scope.orElse(SearchScope.SUB), buildSearchQuery(params, searchOp),
                        attrs.orElse(BASE_SEARCH_ATTRIBUTES).toArray(new String[] {})));
        return result.getSearchEntries();
    }

    private String buildSearchQuery(Map<String, String> params, String searchOp) {
        StringBuilder sb = new StringBuilder();
        sb.append("(").append(searchOp);
        params.entrySet().forEach(e -> sb.append('(').append(e.getKey()).append("=%s)"));
        sb.append(')');
        return String.format(sb.toString(), params.values().toArray());
    }

    private LDAPConnection getConnection() throws LDAPException {
        if (this.connection == null || !this.connection.isConnected()) {
            this.connection = StringUtils.isNotBlank(password) ? new LDAPConnection(host, port, bindDn, password)
                    : new LDAPConnection(host, port);
        }
        return this.connection;
    }

    private boolean isActive() {
        return this.connection != null && this.connection.isConnected();
    }

    @Override
    public void close() throws Exception {
        if (isActive()) {
            this.connection.close();
        }
    }

}
