/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.reports.cli;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.Nullable;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.reports.api.AccountsAPI;
import org.eclipsefoundation.reports.api.DisposableDomainGithubProjectRaw;
import org.eclipsefoundation.reports.api.MediaWikiAPI;
import org.eclipsefoundation.reports.api.MediaWikiAPI.MWQuery;
import org.eclipsefoundation.reports.config.LdapCLIOptions;
import org.eclipsefoundation.reports.dtos.EvtLog;
import org.eclipsefoundation.reports.helper.LDAPHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.auto.value.AutoValue;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchResultEntry;

import picocli.CommandLine;

/**
 * CLI command to generate spam report that looks for entries that have a disposable email domain associated with the
 * account. These accounts will have their uids, mail, groups, and last login time listed to help provide context to
 * whether these users are spam or just questionable.
 * 
 * @author Martin Lowe
 *
 */
@CommandLine.Command(name = "spam-report")
public class SpamLDAPReport implements Runnable {
    public static final Logger LOGGER = LoggerFactory.getLogger(SpamLDAPReport.class);

    private static final DateTimeFormatter CREATE_TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss'Z'");

    // add common options to the command
    @CommandLine.Mixin
    LdapCLIOptions ldapConfig;

    @RestClient
    DisposableDomainGithubProjectRaw gh;
    @RestClient
    MediaWikiAPI mediaWiki;
    @RestClient
    AccountsAPI accounts;
    @Inject
    ObjectMapper om;

    // generate the non-standard CSV mapper object
    final CsvMapper csvMapper = (CsvMapper) new CsvMapper()
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @ActivateRequestContext
    @Override
    public void run() {
        // get the disposable domains from the GH project
        final List<String> value = getDisposableDomains();
        LOGGER.info("Found {} disposable domains to process", value.size());
        // for each domain, run an LDAP query to fetch users that match the domain and parse into output record format
        List<ReportEntry> results = Collections.emptyList();
        try (LDAPHelper ldap = new LDAPHelper(ldapConfig)) {
            // create an index stream to enable better logging, and retrieve all LDAP entries for the given domains
            List<SearchResultEntry> entries = IntStream
                    .range(0, 200)
                    .mapToObj(idx -> fetchAccountsForDomain(idx, value, ldap))
                    .flatMap(i -> i)
                    .collect(Collectors.toList());

            // start conversion process, using logging to demarcate process in results
            LOGGER.info("Beginning conversion of {} entries to final report output", entries.size());
            int fivePercentCount = entries.size() / 20;
            results = IntStream.range(0, entries.size()).mapToObj(e -> {
                // give an indicator every 5% roughly
                if (e % fivePercentCount == 0) {
                    LOGGER.info("{}% of report post-processing complete", (int) Math.ceil(((float) e) / entries.size() * 100));
                }
                // convert LDAP entry to the report entry and return it
                return convertLdapRecord(entries.get(e));
            }).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error("Unable to close LDAP connection", e);
        }
        LOGGER.info("Found {} potentially disposable accounts", results.size());

        // build a string writer, and write the results of the report to the file (too big for console)
        try (StringWriter sw = new StringWriter(); FileWriter fw = new FileWriter("sample.csv")) {
            csvMapper.writer(csvMapper.schemaFor(ReportEntry.class).withHeader()).writeValues(fw).writeAll(results);
            fw.flush();
        } catch (IOException e) {
            LOGGER.error("Error while preparing final report data", e);
        }
        LOGGER.info("Done! The server may now be safely closed");
    }

    /**
     * Using the common LDAP helper, retrieve the disposable domain and do a search for any account that uses that domain.
     * 
     * @param idx the current index of the domain, used for lookup and logging
     * @param value the list of known disposable domains
     * @param ldap the common LDAP helper object for lookups
     * @return the stream of values for accounts using the given domain.
     */
    private Stream<SearchResultEntry> fetchAccountsForDomain(int idx, List<String> value, LDAPHelper ldap) {
        // progress indicator
        if (idx % 100 == 0) {
            LOGGER.info("Processed {} of {} domains", idx, value.size());
        }
        // get the actual domain value
        String d = value.get(idx);
        try {
            // do an LDAP query with the spam domain, retrieving additional properties used in generating the report
            return ldap
                    .search(Map.of("mail", "*" + d), Optional.empty(), Optional.of(Arrays.asList("uid", "mail", "createTimestamp")))
                    .stream();
        } catch (LDAPException e) {
            LOGGER.error("Error while retrieving disposable accounts for domain {}", d);
        }
        return Stream.empty();
    }

    /**
     * Using a basic fetch call, retrieves raw stringified domain list from a Github project that maintains a list of
     * disposable email domains. This list is then serialized into a list of domains to be used in downstream LDAP queries.
     * 
     * @return list of known disposable email domains
     */
    private List<String> getDisposableDomains() {
        String rawDisposableDomains = gh.getRawDomainList(null);
        try {
            return om.readerForListOf(String.class).readValue(rawDisposableDomains);
        } catch (JsonProcessingException e) {
            // cannot continue in this state, throw and end processing
            throw new IllegalStateException(e);
        }
    }

    /**
     * Does conversion from raw LDAP entry to a report entry, using a DB binding to check for login events for the user for
     * last login time, and splitting the DN to get the groups the user is a part of.
     * 
     * Will additionally reach out to the Eclipse Mediawiki to check for contributions, as well as
     * 
     * @param e the LDAP result to convert to a report result entry
     * @return the report entry for the LDAP entry
     */
    private ReportEntry convertLdapRecord(SearchResultEntry e) {
        String creationValue = e.getAttributeValue("createTimestamp");
        String uid = e.getAttributeValue("uid");
        // lookup the user in the DB to find last session time
        EvtLog lastSession = EvtLog.findLastLoginSession(uid);
        String rawPostedMsgCount = accounts.getForumPostsForUser(uid).getPostedMsgCount();
        return ReportEntry
                .builder()
                .setEmail(e.getAttributeValue("mail"))
                .setId(e.getAttributeValue("uid"))
                .setGroups(Arrays
                        .asList(e.getDN().split(","))
                        .stream()
                        .filter(p -> p.startsWith("ou=") || p.startsWith("cn="))
                        .map(p -> p.substring(3))
                        .filter(name -> !name.equalsIgnoreCase(uid))
                        .collect(Collectors.toList()))
                .setCreationDate(StringUtils.isBlank(creationValue) ? null
                        : LocalDateTime.parse(creationValue, CREATE_TIMESTAMP_FORMATTER).atZone(ZoneId.of("UTC")))
                .setLastActive(lastSession == null ? null : lastSession.getEvtDateTime().atZone(ZoneId.of("UTC")))
                .setHasWikiContributions(!mediaWiki.query(new MWQuery(uid)).getQuery().getUsercontribs().isEmpty())
                .setForumPostCount(StringUtils.isNumeric(rawPostedMsgCount) ? Integer.valueOf(rawPostedMsgCount) : 0)
                .setMailingListSubscriptionCount(accounts.getMailingListForUser(uid).getMailingListSubscriptions().size())
                .build();
    }

    /**
     * CSV output row. The JSON property order is how CSV header order is determined.
     * 
     * @author Martin Lowe
     *
     */
    @AutoValue
    @JsonPropertyOrder({ "id", "email", "groups", "creationDate", "lastActive", "hasWikiContributions", "forumPostCount",
            "mailingListSubscriptionCount" })
    @JsonDeserialize(builder = AutoValue_SpamLDAPReport_ReportEntry.Builder.class)
    public abstract static class ReportEntry {
        public abstract String getId();

        public abstract String getEmail();

        public abstract List<String> getGroups();

        @Nullable
        public abstract ZonedDateTime getCreationDate();

        @Nullable
        public abstract ZonedDateTime getLastActive();

        public abstract boolean getHasWikiContributions();

        public abstract int getForumPostCount();

        public abstract int getMailingListSubscriptionCount();

        public static Builder builder() {
            return new AutoValue_SpamLDAPReport_ReportEntry.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setId(String id);

            public abstract Builder setEmail(String email);

            public abstract Builder setGroups(List<String> groups);

            public abstract Builder setCreationDate(@Nullable ZonedDateTime creationDate);

            public abstract Builder setLastActive(@Nullable ZonedDateTime lastActive);

            public abstract Builder setHasWikiContributions(boolean hasWikiContributions);

            public abstract Builder setForumPostCount(int hasForumPosts);

            public abstract Builder setMailingListSubscriptionCount(int hasMailingListSubscriptions);

            public abstract ReportEntry build();
        }
    }
}
